<b>UPDATE:</b> repository moved to BitBucket: https://bitbucket.org/dark_k3y/icscorsair

Archived repository on icscorsair -- tool for pentesting HART networks, that was designed in 2013-2014. These content left only for historic purposes.