#include "abstractio.h"

extern int8 host_mode;
extern uint8 mode;

uint8 rcv_user_array(uint8* array, uint16 max_len){
    uint8 k,len;
    if(host_mode == USB_HOST){ // USB MODE
        return USBUART_1_GetData(array, max_len);
    }else{        
        len = XBEE_GetRxBufferSize();        
        len = len < max_len ? len : max_len;
        for(k = 0; k < len; k++){
            array[k] = XBEE_ReadRxData();                      
        }
        CyDelay(1);
        return len;
    }
}

void send_touser_byte(uint8 c){
    if(host_mode == USB_HOST){ // USB MODE
        while(USBUART_1_CDCIsReady() == 0u);
        USBUART_1_PutChar(c);
    }else{
        XBEE_PutChar(c);
        while(XBEE_GetTxBufferSize() != 0);   
    }        
}

void send_touser_array(uint8* s, uint16 len){
    if(host_mode == USB_HOST){ // USB MODE
        while(USBUART_1_CDCIsReady() == 0u);
        USBUART_1_PutData(s, len);
    }else{
        XBEE_PutArray(s, len+1);
        while(XBEE_GetTxBufferSize() != 0);   
    }        
}


void send_toline_byte(uint8 c){
    if(mode == FSK_HART_MODE){ // HART MODE
        UART_1_PutChar(c);
        while(UART_1_GetTxBufferSize() != 0);        
    }else{
        RS485_PutChar(c);
        while(RS485_GetTxBufferSize() != 0);        
    }
}

void send_toline_array(uint8* s, uint16 len){    
    uint16 i,k,c;
    if(mode == FSK_HART_MODE){ // HART MODE
        if(len <= 255){  // HART disallow packets > 255, so we'll make a "cheat"
            UART_1_PutArray(s, len);
            while(UART_1_GetTxBufferSize() != 0);
        }else{
            k = len / 255;
            c = len % 255;
            for(i = 0; i < k; i++){
                UART_1_PutArray(s + i*255, 255);
                while(UART_1_GetTxBufferSize() != 0);
                CyDelay(1);
            }
            UART_1_PutArray(s + k*255, c);
            while(UART_1_GetTxBufferSize() != 0);
        }
    }else{
        RS485_PutArray(s, len);
        while(RS485_GetTxBufferSize() != 0);
    }
}