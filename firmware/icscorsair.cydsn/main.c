#include "icscorsair.h"


uint8 result; 
uint8 submode;
uint16 xbee_size;
uint8 host_mode;
uint8 mode;
uint16 buffer_size;
// mode changing string: 0x1B (Esc), 0x6D (m), 0x43 (Shift+C) + mode number in ascii
uint8 changemode;

CY_ISR(FSK_RX_int)
{
    //uint8 cbyte;
    //uint8 k;
    /*  Place your Interrupt code here. */
    /* `#START isr_1_Interrupt` */
    
    if(mode == FSK_HART_MODE){
        //cbyte = UART_2_ReadRxData();
        //while(USBUART_1_CDCIsReady() == 0u);
        //USBUART_1_PutChar(cbyte);        
        send_touser_byte(UART_2_ReadRxData());
        /*k = UART_2_GetRxBufferSize();
        while( k-- > 0 ){
            cbyte = UART_2_ReadRxData();
            send_touser_byte(cbyte);
        }*/
    }
    /* `#END` */

    /* PSoC3 ES1, ES2 RTC ISR PATCH  */ 
    #if(CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC3)
        #if((CYDEV_CHIP_REVISION_USED <= CYDEV_CHIP_REVISION_3A_ES2) && (isr_1__ES2_PATCH ))      
            isr_1_ISR_PATCH();
        #endif /* CYDEV_CHIP_REVISION_USED */
    #endif /* (CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC3) */
}

CY_ISR(RS485_RX_int)
{
    //uint8 cbyte;
    //uint8 k;
    /*  Place your Interrupt code here. */
    /* `#START isr_1_Interrupt` */
    
    if(mode == RS_485_MODE){
        //cbyte = UART_2_ReadRxData();
        //while(USBUART_1_CDCIsReady() == 0u);
        //USBUART_1_PutChar(cbyte);        
        send_touser_byte(RS485_ReadRxData());
        /*k = UART_2_GetRxBufferSize();
        while( k-- > 0 ){
            cbyte = UART_2_ReadRxData();
            send_touser_byte(cbyte);
        }*/
    }
    /* `#END` */

    /* PSoC3 ES1, ES2 RTC ISR PATCH  */ 
    #if(CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC3)
        #if((CYDEV_CHIP_REVISION_USED <= CYDEV_CHIP_REVISION_3A_ES2) && (isr_1__ES2_PATCH ))      
            isr_1_ISR_PATCH();
        #endif /* CYDEV_CHIP_REVISION_USED */
    #endif /* (CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC3) */
}



int main()
{
    uint8 cbyte;
    
    host_mode = USB_HOST;
    changemode = 0;
    mode = BIN_CONF_MODE;
    submode = 0;
    xbee_size = 0;
    buffer_size = 1024;
       
    HW_Init();
    
    CyGlobalIntEnable;
        
    // reading modes presets from EEPROM
    read_EEPROM_Config();
    
    // after reading modes we knew, should we init USB or not.
    if(host_mode == USB_HOST)
        USB_init();
        
    // init XBEE
    if(host_mode == XBEE_HOST)
        XBEESLOT_Init();
    
    // setting ISR vectors
    // HART
    isr_1_Start();
    isr_1_SetVector(FSK_RX_int); //Set the Vector to the ISR.
    UART_2_SetRxInterruptMode(UART_2_RX_STS_FIFO_NOTEMPTY);
    // RS-485
    isr_2_Start();
    isr_2_SetVector(RS485_RX_int); //Set the Vector to the ISR.
    RS485_SetRxInterruptMode(RS485_RX_STS_FIFO_NOTEMPTY);
    
    //FSK_Buffer_Sleep();
    //FSK_Buffer_Stop();    
    
    //if(usb_enabled)
    
    //host_mode = USB_HOST;
    
    DBG_Write(0);
    //RLIN_Write(1);
      
    for(;;)
    {

        //HRTS_Write(0);        
        //HRTS_Write(0);
        //while(RS485_GetRxBufferSize() > 0);
        //    send_touser_byte(RS485_ReadRxData());        
        //send_toline_data("Hello World!", 12);
        //send_toline_char('A');
        //CyDelay(1);
        //UART_1_PutString("Hello World!");        
        //continue;
        //RS485_PutString("Hello world!");
        //continue;
        
        switch(mode){
            case TXT_CONF_MODE:
                text_conf_mode();
                break;
            
            case RS_485_MODE:
            case FSK_HART_MODE:
                line_mode();
                break;
            
            case BIN_CONF_MODE:                
            default:
                bin_conf_mode();
                break;            
        }
                    
        //USBUART_1_PutString("!!!!!");
        //CyDelay(100);
        //DBG_Write(0);
    }
}

