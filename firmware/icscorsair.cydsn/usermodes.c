#include "usermodes.h"

extern uint8 host_mode;
extern uint8 mode;
extern uint8 changemode;
extern uint8 submode;
extern uint16 xbee_size;
extern uint16 buffer_size;

uint8 buffer[MAX_BUFFER + 1];
char modechars[] = {0x1B, 0x6D, 0x43, 0x00, 0x00};

void print_hex(uint8 c){
    register unsigned int u = c;
    register unsigned int t;
    while (u) {
		t = u % 16;
		if( t >= 10 )
			t += 'A' - '0' - 10;
		send_touser_byte(t + '0');
		u /= 16;
	}
}


void print_menu(){
    send_touser_array((uint8*)ICSCORSAIR_BANNER, 22);
    send_touser_array((uint8*)"       ***text-mode menu****       \r\n", 37);
    send_touser_array((uint8*)"m<mode> -- change current mode     \r\n", 37);
    send_touser_array((uint8*)"8<sp>   -- preset RS-485 speed     \r\n", 37);
    send_touser_array((uint8*)"J/j     -- start/stop HART line JAM\r\n", 37);
    send_touser_array((uint8*)"R/r     -- toggle RS-485 resitor   \r\n", 37);
    send_touser_array((uint8*)"s<mode> -- set default start mode  \r\n", 37);
    send_touser_array((uint8*)"U/u     -- ena/disable USB on start\r\n", 37);
    send_touser_array((uint8*)"E       -- print EEPROM in hex     \r\n", 37);
    send_touser_array((uint8*)"X       -- get XBee init strings   \r\n", 37);
    send_touser_array((uint8*)"?       -- print this menu         \r\n", 37);
    send_touser_byte('>');
}

void hart_jam(){
    uint8 stop = 0;
    AMux_1_Set(0); HRTS_Write(0);
    while(stop == 0){
        UART_1_WriteTxData('\x00');
        if(get_user_data_count()){
            if(rcv_user_byte())   
                stop = 1;
        }
    }
    mode = 2;
    AMux_1_Disconnect(0); HRTS_Write(1);
}

void text_conf_mode(){
    uint16 len;
    uint16 i;
    uint16 j;
    uint16 k;    
    uint8 tmp[80];
    reg8 * p_eeprom = (reg8 *)CYDEV_EE_BASE;    
           
    while(is_ready_user_data() > 0){ //   
      len = rcv_user_array(buffer, 64);      
      for(k = 0; k < len; k++){        
        if(changemode > 0){            
            if((changemode == 1 && buffer[k] == 0x6D) || (changemode == 2 && buffer[k] == 0x43)){                
                changemode++;
            }else if(changemode == 3 && buffer[k] - 48 < 4){                
                mode = buffer[k] - 48;
                return;
            }else
                changemode = 0;            
                
        }else if(submode == 0){
            switch(buffer[k]){
                case 0x1B:
                    changemode = 1;
                break;
                
                case 'J':
                    send_touser_array((uint8*)"Starting HART JAM, press j to stop.\r\n", 37);
                    submode = 0;
                    hart_jam();
                    return;
                break;
                
                case 'm':
                case 's':
                case '8':
                    submode = buffer[k];  
                break;
                
                case 'R':
                    RLIN_Write(1);
                break;
                
                case 'r':
                    RLIN_Write(0);
                break;
                
                case 'U':
                    EEPROM_ByteWrite(0x01, 0, 2);
                break;
                
                case 'u':
                    EEPROM_ByteWrite(0x00, 0, 2);
                break;
                
                case 'E':
                    send_touser_array("\r\n", 2);
                    for(i = 0; i < CYDEV_EE_SIZE; i+=16){
                        sprintf(tmp, "0x%X ", CYDEV_EE_BASE + i);
                        send_touser_array(tmp, 7);
                        for(j = 0; j < 16; j++)
                            if( p_eeprom[i] >= 32 && p_eeprom[i] <= 126)
                                send_touser_byte(p_eeprom[i+j]);
                            else
                                send_touser_byte('.');  
                        /*send_touser_byte(' ');  
                        for(j = 0; j < 16; j++){
                            print_hex(p_eeprom[i+j]);
                            send_touser_byte(' ');  
                        }*/                        
                        send_touser_array("\r\n", 2);
                    }   
                break;
                
                case 'X':
                    for(i = 48; i < CYDEV_EE_SIZE && p_eeprom[i] != '\0' && p_eeprom[i] != '\xFF'; i++){
                        send_touser_byte(p_eeprom[i]);                            
                    }   
                break;
                
                case '?':
                default:
                   print_menu();  
                break;
            }
        }else{
            switch(submode){
                case 'm':
                    if(buffer[k] - 48 >=0 && buffer[k] - 48 < 4){
                        mode = buffer[k] - 48;
                        send_touser_byte(buffer[k]);
                        submode = 0;
                        return;
                    }else
                        send_touser_array((uint8*)"\r\nInvalid mode.\r\n", 14);
                break;
                case 's':
                    if(buffer[k] - 48 >=0 && buffer[k] - 48 < 4){
                        // write to EEPROM
                        EEPROM_ByteWrite(buffer[k] - 48, 0, 1);
                    }else
                        send_touser_array((uint8*)"\r\nInvalid mode.\r\n", 14);
                break;
                case '8':
                    if(!(buffer[k] - 48 >=0 && set_EE_rs485_clock(buffer[k])))
                        send_touser_array((uint8*)"\r\nInvalid speed.\r\n", 18);
                break;
            }
            send_touser_byte('>');
            submode = 0;
        }                
      }  
    }    
}

void bin_conf_mode(){
    uint16 len;
    uint16 k;
    
    while(is_ready_user_data()){
      len = rcv_user_array(buffer, 64);
   
      for(k = 0; k < len; k++){          
        if(changemode > 0){      
            if((changemode == 1 && buffer[k] == 0x6D) || (changemode == 2 && buffer[k] == 0x43)){                
                changemode++;
            }else if(changemode == 3 && buffer[k] - 48 < 4){                
                mode = buffer[k] - 48;
                return;
            }else
                changemode = 0;      
        } 
        else if(buffer[k] == 'J')
            hart_jam();                    
        else if(buffer[k] == 0x1B)
            changemode = 1;
        else if(submode == 0)
            submode = buffer[k];        
        else if(submode == 0xFB){
            if(xbee_size > 4096 || buffer[k] == 0xFC){
                submode = 0;   
            }else{                
                //write cbyte to EEPROM by xbee_size
                xbee_size++;
            }
            
        }else{
            switch(submode){                
                case 0xFE:
                    submode = 0;
                    if(buffer[k]<4){
                        // set startmode in EEPROM
                        EEPROM_ByteWrite(buffer[k], 0, 1);
                        send_touser_byte(0x01);
                    }else{
                        send_touser_byte(0x00);
                    }                    
                    break;
                case 0xFD:
                    submode = 0;
                    if(buffer[k]<2){
                        // set USBmode in EEPROM
                        EEPROM_ByteWrite(buffer[k], 0, 2);
                        send_touser_byte(0x01);
                    }else{
                        send_touser_byte(0x00);
                    }      
                    break;
                case 0xFA:
                    if(buffer[k]<4){
                        send_touser_byte(0x01);
                        mode = buffer[k];
                        return;
                    }else{
                        send_touser_byte(0x00);
                    }                    
                    break;
                case 0x85:
                    submode = 0;
                    send_touser_byte(set_EE_rs485_clock(buffer[k]));
                    break;
                    
                case 0x8E:
                    submode = 0;
                    if(buffer[k]<2){
                        RLIN_Write(buffer[k]);
                        send_touser_byte(0x01);
                    }else{
                        send_touser_byte(0x00);
                    }      
                    break;
                default:
                    send_touser_byte(0x00);
                    break;
            }
        }
        //sprintf(str, "!%X!\n",cbyte);
            
            /*USBUART_1_PutChar('!');
            while(USBUART_1_CDCIsReady() == 0u);
            USBUART_1_PutChar(cbyte);
            while(USBUART_1_CDCIsReady() == 0u);*/
      }      
    }
}

uint8 flow_check_control_seq(uint8* buffer){            
            if(changemode == 0 && buffer[0] == 0x1B){   
                modechars[0] = 0x1B;
                changemode = 1;
            }else if(changemode == 1 && buffer[0] == 0x6D){
                modechars[1] = 0x6D;
                changemode = 2;
            }else if(changemode == 2 && buffer[0] == 0x43){
                modechars[2] = 0x43;
                changemode = 3;
            }else if(changemode == 3 && buffer[0] - 48 < 4){            
                mode = buffer[0] - 48;
                changemode = 0;
                return 1;
            }else{                
                modechars[changemode] = buffer[0];
                changemode++;
                modechars[changemode] = '\0';
                send_toline_array((uint8 *)modechars, changemode);
                changemode = 0;
            }   
            
            return 0;
}

void line_mode(){
    uint8 cbyte = 0;
    uint16 i;
    uint16 len;
    uint16 k;    
    
    // FSK reciever       
    if(mode == FSK_HART_MODE){
        while(UART_2_ReadRxStatus() == UART_2_RX_STS_FIFO_NOTEMPTY)
            send_touser_byte(UART_2_ReadRxData());               
    }else{
    // RS485 reciever
        while(RS485_ReadRxStatus() == RS485_RX_STS_FIFO_NOTEMPTY)
            send_touser_byte(RS485_ReadRxData());   
    }
    
    // minimum size of HART packet is preamble + 5 bytes > 4 bytes. 
    while(is_ready_user_data()){
        len = get_user_data_count();
        
        if((host_mode == USB_HOST && len > 1) || host_mode == XBEE_HOST){
            start_line_output();         
            len = 0;
            
            while(get_user_data_count()){
                //send_touser_byte(moredata);
                k = rcv_user_array(buffer + len, 64); //rcv_user_array(buffer + len, 64);                
                len += k;
                //send_touser_byte(k);
                //send_touser_byte(len);            
                
                if(len + USER_DATA_BLOCK > MAX_BUFFER)
                    break;
                
                if(mode == FSK_HART_MODE)
                    CyDelay(1);
            }      
            
            if(buffer[0] == 0x1B){
                if(buffer[1] == 0x6D && buffer[2] == 0x43 && buffer[3] - 48 < 4){
                    mode = buffer[3] - 48;
                    changemode = 0;
                    return;
                }
            }
            
            if(mode == FSK_HART_MODE){
                if(get_user_data_count() == 0){
                    len++;
                    buffer[len] = '\0';
                }
            }
            
            send_toline_array((uint8 *)buffer, len);                
            finish_line_output();
            //send_touser_byte('A');
            /*UART_1_WriteTxData(cbyte);
            i = 0;
            while(is_ready_user_data())
                buffer[i++] = rcv_user_data();
            send_toline_data(buffer, i);                */
        }else{ 
            len = rcv_user_array(buffer, 1);
            if(flow_check_control_seq(buffer) == 1)
                return;
        }

    }
}
