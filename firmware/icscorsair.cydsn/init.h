#ifndef __INIT_H
#define __INIT_H
    
#include "icscorsair.h"
    
void HW_Init(void);    
    
void DMA_Initalization(void);

uint8 set_EE_rs485_clock(uint8 c);

void XBEESLOT_Init();

void USB_init();

void read_EEPROM_Config();

#endif