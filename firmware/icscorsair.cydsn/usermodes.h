#ifndef __USERCONFIG_H
#define __USERCONFIG_H

#include "icscorsair.h"
#include "abstractio.h"

void text_conf_mode();

void bin_conf_mode();

void line_mode();

#endif