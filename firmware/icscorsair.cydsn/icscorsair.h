#ifndef __ICSCORSAIR_H
#define __ICSCORSAIR_H
    
#define ICSCORSAIR_BANNER "ICSCorsair v.0.03.2\r\n"
    
#include <project.h>
#include <stdio.h>
#include "init.h"
#include "abstractio.h"
#include "usermodes.h"

    
#define BIN_CONF_MODE 0
#define TXT_CONF_MODE 1
#define FSK_HART_MODE 2
#define RS_485_MODE 3
    
#define XBEE_HOST 0
#define USB_HOST 1
    
#define MAX_BUFFER 1024
#define USER_DATA_BLOCK 64
    
#endif


