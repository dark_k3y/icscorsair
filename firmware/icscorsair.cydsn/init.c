#include "init.h"

extern int8 host_mode;
extern uint8 mode;

uint8 sine_table[64]= { 95,104,113,122,131,139,147,155,162,168,173,178,182,185,188,
						189,190,189,188,185,182,178,173,168,162,155,147,139,131,122,
						113,104,95,85,76,67,58,50,42,34,27,21,16,11,7,4,1,0,0,0,1,4,
						7,11,16,21,27,34,42,50,58,67,76,85};

                  // 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 460800 
uint16 dividers[] = {2500, 1250, 625, 312, 156, 78, 39, 19, 5 };

void HW_Init(void){
    
    EEPROM_Enable();
 
    FSK_Buffer_Start();
	FSK_Generator_Start();
	
    UART_1_Start();
    RS485_Start();
    
    BPF_OpAmp_Start();
	BPF_ZCD_COMP_Start();
	LPF_ZCD_COMP_Start();
	Delay_ShiftReg_Start();
	LPF_PGA_Start();
	UART_2_Start();
    XBEE_Start();
    
    RS485Clk_SetDivider(2500); // 1.2kHz

	DMA_Initalization();
    
    LED_Write(0);
    RLIN_Write(0);
    HRTS_Write(1);
    AMux_1_Start();
}

void DMA_Initalization(void)
{
	uint8 td0, MyChannel;

	#if (defined(__C51__))
	MyChannel=Sinetable_DMA_DmaInitialize(1,	/*Send 1 Byte Per DRQ Request*/
								  		  1,	/*Each Request Must be indiviually triggered by DRQ*/
								  		  0,	/*NA*/
								  		  0);	/*NA*/
	#else
	MyChannel=Sinetable_DMA_DmaInitialize(1,	/*Send 1 Byte Per DRQ Request*/
								  		  1,	/*Each Request Must be indiviually triggered by DRQ*/
								  		  HI16(sine_table),	/*NA*/
								  		  HI16(FSK_Generator_viDAC8__D));	/*NA*/
	#endif
							  
	/*Allocated TDs for the DMA*/
	td0=CyDmaTdAllocate();
	
	/*configure the TDs*/
	/*TD0 Transfers 64 Bytes, after each byte the source address is incremented*/
	CyDmaTdSetConfiguration(td0,64,td0,TD_INC_SRC_ADR);
	
	/*Assign the addresses for TDs
	/*TD0 Transfers data from the sine table to the VDAC data register*/
	CyDmaTdSetAddress(td0, LO16((uint32)sine_table), LO16((uint32)FSK_Generator_viDAC8__D) );
	
	/*Set the intialize TD*/
	CyDmaChSetInitialTd(MyChannel, td0);
	/*enable the DMA*/
	CyDmaChEnable(MyChannel,1);
}

uint8 set_EE_rs485_clock(uint8 c){
    if(c < 10){
        RS485Clk_SetDivider(c);
        return 0;
    }else
        return 0;
}


void XBEESLOT_Init(){
    reg8 * p_eeprom = (reg8 *)CYDEV_EE_BASE;
    uint16 i = 48;   
    
    /*CyDelay(1000);
    XBEE_PutString("\r\n+STWMOD=0\r\n");
    XBEE_PutString("\r\n+STNA=modem\r\n");
    XBEE_PutString("\r\n+STAUTO=0\r\n");
    XBEE_PutString("\r\n+STOAUT=1\r\n");
    XBEE_PutString("\r\n+STPIN=0000\r\n");
    CyDelay(2000);
    XBEE_PutString("\r\n+INQ=1\r\n");   
        
    return;*/
    
    while(p_eeprom[i] != '\0' && p_eeprom[i] != '\xFF' && i < 2000){ // 2048/2
        if(p_eeprom[i] == 'd'){
            CyDelay((p_eeprom[i+5] - 48)*1000);
            i+=7;
        }else if(p_eeprom[i] == 's'){
            i+=5;
            while(p_eeprom[i] != '\0' && p_eeprom[i] != '\xFF' && i < 1999 && p_eeprom[i] != '\n' && p_eeprom[i] != '\r'){
                if(p_eeprom[i] == '\\'){
                    switch(p_eeprom[i+1]){
                        case '\\':
                            XBEE_PutChar('\\');
                            break;
                        case 'n':
                            XBEE_PutChar('\n');
                            break;
                        case 'r':
                            XBEE_PutChar('\r');
                            break;
                    }
                    i++;
                }else
                    XBEE_PutChar(p_eeprom[i]);
                i++;
            }
        }
        i++;
    }
    
    XBEE_ClearRxBuffer();
    XBEE_ClearTxBuffer();
    
    //XBEE_PutString("EEPROM read"); 
}

void USB_init(){
    unsigned long start_time = 0;
    
    USBUART_1_Start(0u, USBUART_1_3V_OPERATION);
    
    while(!USBUART_1_GetConfiguration() && start_time++ < 500000);
    
    if(USBUART_1_GetConfiguration()){
        USBUART_1_CDC_Init();
        host_mode = USB_HOST;
        CyDelay(1);        
    }else
        host_mode = XBEE_HOST;
}

void read_EEPROM_Config(){
    reg8 * p_eeprom = (reg8 *)CYDEV_EE_BASE;
    
    if(p_eeprom[0] != 0x01 || p_eeprom[1] == 0xFF || p_eeprom[2] == 0xFF){
        // no (or corrupted) EEPROM config, write default        
        EEPROM_ByteWrite(0x01, 0, 0);
        EEPROM_ByteWrite(0x00, 0, 1);
        EEPROM_ByteWrite(0x01, 0, 2);
    }else{        
        mode = (uint8) p_eeprom[1];
        host_mode = (uint8) p_eeprom[2];      
    }
}