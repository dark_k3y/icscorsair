#ifndef __ABSTRACTIO_H
#define __ABSTRACTIO_H

#include "icscorsair.h"

#define is_ready_user_data()    (host_mode == USB_HOST ? USBUART_1_DataIsReady() : XBEE_GetRxBufferSize())
#define get_user_data_count()   (host_mode == USB_HOST ? USBUART_1_GetCount() : XBEE_GetRxBufferSize())
    
#define start_line_output()     if(mode == FSK_HART_MODE){ AMux_1_Set(0); HRTS_Write(0); CyDelay(1); }
#define finish_line_output()    if(mode == FSK_HART_MODE){ AMux_1_Disconnect(0); HRTS_Write(1); }
    
#define rcv_user_byte()         (host_mode == USB_HOST ? USBUART_1_GetChar() : XBEE_ReadRxData())

uint8 rcv_user_array(uint8* array, uint16 max_len);

void send_toline_array(uint8*, uint16 len);

void send_toline_byte(uint8 c);

void send_touser_array(uint8*, uint16 len);

void send_touser_byte(uint8 c);

    
#endif