require 'serialport'
require 'hartparser.rb'

if ARGV.length < 2
	puts "Usage hart_sniff.rb <COM port> <port baud rate>."
	exit
end

device = ARGV[0]
speed = ARGV[1]
sp = SerialPort.new(device, baud = speed.to_i())
sp.write "\x1BmC2"
sleep(0.01)

def process_packet(packet)
	cmdrq = Hartrequestparser.new
	cmdrq.debug = true
	cmdrs = Hartresponseparser.new
	cmdrs.debug = true

	puts bin_to_hex(packet)
	h = Hartpdu.new(packet)	
	
	#reserved for future filters
	if not [].include?(h.command)
		puts h.inspect
		# parsing only standard commands
		if h.command < 128
			if h.get_frame_type() == Hartpdu::HART_STX_FRAME
				cmdrq.parse(h.command, h.data)			
			else
				cmdrs.parse(h.command, h.data)			
			end
		end
	end
end

i = 0
array1 = []
prev = -1
while (cbyte = sp.read)
		if prev == -1
			prev = cbyte
			array1[i] = cbyte
			i = i + 1
		else	
			if prev != 255 and cbyte == 255				
				prev = cbyte
				process_packet array1.pack("c*")
				array1 = []                     
				i = 0
			else 
				prev = cbyte
				array1[i] = cbyte
				i = i + 1
			end				
		end
end



