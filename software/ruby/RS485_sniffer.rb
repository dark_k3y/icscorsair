require 'serialport'

if ARGV.length < 3
	puts "Usage RS485_sniffer.rb <COM port> <port baud rate> <out_file>."
	exit
end

device = ARGV[0]
speed = ARGV[1]
outfile = ARGV[2]
sp = SerialPort.new(device, baud = speed.to_i())
sp.write "\x1BmC3"
sleep(0.01)

f = open(outfile, 'a')
f.close()

while (cbyte = sp.read)
	open(outfile, 'a') { |f|
 	 f.puts cbyte.chr
	}	        	
end



