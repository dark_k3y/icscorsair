##
# This module requires Metasploit: http//metasploit.com/download
# Current source: https://github.com/rapid7/metasploit-framework
##


require 'msf/core'
#require 'hartparser'
#require 'rubyserial'

class Metasploit3 < Msf::Auxiliary

  include Msf::Auxiliary::Report

  def initialize
    super(
      'Name'        => 'HART change RTU device longtag',
      'Description' => %q{
	The HART Communications Protocol (Highway Addressable Remote Transducer Protocol) is
	a digital industrial automation protocol. HART is used for communicating between master 
	(i.e. host computer with HART modem, PLC or HART field communicator) and slave (RTU 
	device, Remote Transmitter Unit, like transmitter or actuator). This auxiliary script
	is made for scanning HART network and find all slave field devices on the bus.
      },
      'Author'      => 'Alexander Bolshev (@dark_k3y)',
      'License'     => MSF_LICENSE,
      'References'  =>
        [
          [ 'URL', 'http://www.slideshare.net/dark_k3y/hart-insecurity-how-one-transmitter-can-compromise-whole-plant' ],
	  [ 'URL', 'http://www.slideshare.net/dark_k3y/hart-as-an-attack-vector-from-current-loop-to-application-layer'],
        ],
      'DisclosureDate' => '07 Aug 2014')

      register_options([
	OptInt.new('LOWPOLLINGID', [ true,  "the lowest network address", 1]),
	OptInt.new('HIGHPOLLINGID', [ true,  "the highest network address", 63]),
	OptString.new('PORT', [ true,  "serial port of modem or ICSCorsair (e.g. COM1 or /dev/ttyACM0)", '/dev/ttyACM0']),
	OptInt.new('BAUD', [ true,  "serial port baud rate", 1200]),
	OptEnum.new('DEVICE', [true, 'connection device', 'ICSCorsair', ['ICSCorsair', 'modem'] ])
      ])

    deregister_options('HOST')
    deregister_options('RHOST')
  end

  def modem_open()
	sp = SerialPort.new datastore['PORT'], datastore['BAUD']	
	sp.read_timeout = 10
	return sp
  end

  def modem_close(sp)
	sp.close()
  end

  def set_mode(sp, mode)
	sp.write "\x1BmC" + mode.to_s()
	sleep(0.01)
  end

  def send_scan_probe(a)
	cmdrs = Hartresponseparser.new
	cmdrs.debug = true
	rs = Hartpdu.new
	rs.preamble = 20
	rs.delimeter = Hartpdu::HART_STX_FRAME
	rs.address = a 
	rs.response = 0
	rs.status = 0
	rs.command = 6
	rs.expbytes = []
	rs.set_addrtype(0)
	sp.write rs.to_str()

	i = 0
	while i < 30
                income = sp1.read
                i = i + 1
                sleep(0.001)
                next if income.length < 1
                puts bin_to_hex(income)
		h = Hartpdu.new(income)	
		cmdrs.parse(h.command, h.data)
        end
  end

  def run
    begin
      debug = true

      sp = modem_open

      set_mode sp, 2 if datastore['DEVICE'] == 'ICSCorsair' 
      sleep(0.1)

      for i in datastore['LOWPOLLINGID']..datastore['HIGHPOLLINGID']
	send_scan_probe(i)
      end

      modem_close sp
    end
  end
end

